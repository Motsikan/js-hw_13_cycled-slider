const coll = document.querySelectorAll(".image-to-show");
const element = document.querySelector(".images-wrapper");
// const father = document.querySelector(".images-wrapper");
const playButton = document.querySelector(".button-one");
const stopButton = document.querySelector(".button-two");

let slideIndex = 0;
let playing = true;

showSlides();
function showSlides() {
  if (playing) {
    let i;
    let slides = document.getElementsByClassName("image-to-show");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.visibility = "hidden";
    }
    slideIndex++;
    
    if (slideIndex > slides.length) {
      slideIndex = 1;
    }
    slides[slideIndex - 1].style.visibility = "";
    setTimeout(showSlides, 3000);
  }
}

function slideShowStart() {
  if (!playing) {
    playing = true;
    showSlides();
  }
}

function slideShowStop() {
  if (playing) {
    playing = false;
  }
}

playButton.addEventListener("click", slideShowStart);
stopButton.addEventListener("click", slideShowStop);

